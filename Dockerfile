FROM debian:bullseye-slim
MAINTAINER https://bitbucket.org/bluesquall/docker-android-studio/issues

# manually make V* man1 *V to prevent openjdk dpkg error
RUN mkdir -p /usr/share/man/man1 \
 && apt-get update && apt-get install --yes --no-install-recommends \
      ca-certificates \
      curl \
      default-jdk \
      libnss3 \
      libpulse0 \
      sudo \
      unzip \
      xorg \
      zip \
      zsh \
 && apt-get autoremove --yes && apt-get autoclean && apt-get clean

ARG USERNAME=artoo
ARG USER_UID=18242
ARG USER_GID=${USER_UID}

RUN groupadd --gid ${USER_GID} ${USERNAME} \
 && useradd --uid ${USER_UID} --gid ${USER_GID} --shell /bin/zsh --create-home ${USERNAME} \
 && echo ${USERNAME} ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/${USERNAME} \
 && chmod 0440 /etc/sudoers.d/${USERNAME}

ENV PATH ${PATH}:/opt/android-studio/bin
ENV STUDIO_URL https://redirector.gvt1.com/edgedl/android/studio/ide-zips/3.6.1.0/android-studio-ide-192.6241897-linux.tar.gz
# ^ see https://developer.android.com/studio/archive

WORKDIR /opt
RUN curl -SL ${STUDIO_URL} | tar -xz

USER ${USERNAME}
ENV HOME /home/${USERNAME}
WORKDIR ${HOME}

CMD /opt/android-studio/bin/studio.sh

